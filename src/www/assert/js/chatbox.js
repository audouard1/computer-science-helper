$(function () {
   displayChat();

    $('#send').click(function () {
            const message = $('#message').val();

            $.ajax({
                type: 'POST',
                url: 'htbin/chatsend.py',
                timeout: 3000,
                data: {
                    msg: message
                },
                success: function (request) {
                    console.log(request);
                    if(request.num !== 0){
                        $('#error').text( request.msg);
                        $('#success').text( '');
                    }else{
                        $('#success').text( request.msg);
                        $('#error').text( '');
                    }
                    displayChat();
                },
                error: function () {
                    $('#error').text( 'erreur envoi maessage');
                }
            });
    });

    function displayChat(){
        $.ajax({
            type: 'GET',
            url: 'htbin/chatget.py',
            timeout: 3000,
            success: function (request) {
                console.log(request);
                $('#chatbox').empty();
                for (const data of request) {
                    console.log(data);
                    $('#chatbox').append('<p>' + data.date + ' ' + data.time +'<br>'+ data.user + '<br>' + data.msg + '</p>');
                }
            },
            error: function () {
                $('#error').text( 'erreur récupération des messages');
            }
        })
    }
    setInterval(displayChat, 10000);
});
