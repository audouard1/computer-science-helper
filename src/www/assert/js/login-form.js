
const submit = document.getElementById('btn-submit');
const form = document.getElementById('login-form');
const message = document.getElementById('message');
const password = document.getElementById('password');

function submitForm() {
    const request = new XMLHttpRequest();

    request.addEventListener('readystatechange', function () {

        if (request.readyState === XMLHttpRequest.DONE) {

            form.hidden = request.responseText.startsWith('Bonjour');
            message.hidden = false;
            message.innerText = request.responseText;
        }
    });

    const formData = new FormData(form);


    console.log(formData);
    request.open('post', 'htbin/login.py');


    request.send(formData);
}

submit.addEventListener('click', () => {
    submitForm();
});

password.addEventListener('keyup', function (e) {
    if(e.altKey === "Enter"){
        submitForm();
    }
});

form.addEventListener("submit", function (event) {
    event.preventDefault();

}, false);
