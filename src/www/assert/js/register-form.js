const formContent = {
    lastname: document.getElementById('last_name'),
    firstname: document.getElementById('first_name'),
    birthdate: document.getElementById('birthday'),
    username: document.getElementById('id'),
    userpwd: document.getElementById('password'),
    useremail: document.getElementById('mail'),
    submit: document.getElementById('btn-submit'),


};
const form = document.getElementById('register-form');
/*
// AJAX
formContent.submit.addEventListener('click', () => {
    const request = new XMLHttpRequest();
    request.onload = () => {
        console.log(request.responseText);
    };

    var formData = new FormData();
    formData.append('lastname', formContent.lastname.value);
    formData.append('firstname', formContent.firstname.value);
    formData.append('birthdate', formContent.birthdate.value);
    formData.append('username', formContent.username.value);
    formData.append('userpwd', formContent.userpwd.value);
    formData.append('useremail', formContent.useremail.value);


    console.log(formData);
    request.open('post', 'htbin/register.py');
    request.setRequestHeader('Content-type', 'application/x-formContent-urlencoded'); //key value pair stream
    request.send(formData);

});*/


const emailRegExp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
const passwordRegExp = /^(.{0,7}|[^0-9]*|[^A-Z]*|[^a-z]*)$/;
const dateRegExp = /^(?<day>\d{2})\/(?<month>\d{2})\/(?<year>\d{4})$/

formContent.useremail.addEventListener("keyup", function () {
    validElem(formContent.useremail, emailRegExp, "mail non valide", false, -1,
        "adresse mail", "error_mail");
});

formContent.lastname.addEventListener("keyup", function () {
    validElem(formContent.lastname, /^.*$/, "nom non valide", false, -1,
        "nom", "error_last_name");
});

formContent.firstname.addEventListener("keyup", function () {
    validElem(formContent.firstname, /^.*$/, "prenom non valide", false, -1,
        "prenom", "error_first_name");
});

formContent.username.addEventListener("keyup", function () {
    validElem(formContent.username, /^.*$/, "nom d'utilisateur doit contenir au moins six caractèrese", false, 6,
        "identifiant", "error_id");
});

formContent.userpwd.addEventListener('keyup', function () {
    validElem(formContent.userpwd, passwordRegExp, "mot de passe doit contenir au moins 8 caractère ainsi qu'une minuscule, une majuscule et un chiffre", false, -1,
        "mot de passe", "error_password", true);
});

formContent.birthdate.addEventListener("keyup", function () {
    validDate(formContent.birthdate);
});


let validDate = function (birthdate) {
    const error = document.getElementById('error_birthday');
    if (dateRegExp.test(birthdate.value)) { /* ne passe jamais dans ce if, pas normal...*/
        let datereg = dateRegExp.exec(birthdate.value);
        let day = Number(datereg.groups.day);
        let month = Number(datereg.groups.month);
        let year = Number(datereg.groups.year);


        let date = new Date(year, month - 1, day);

        let dateYear = date.getFullYear();
        let dateMon = date.getMonth();
        let dateDay = date.getDate();

        dateMon += 1;

        if (dateYear === year && dateMon === month && dateDay === day) {
            birthdate.className = "valid";
            error.innerHTML = "";
            error.className = "error";
            return true;
        } else {
            error.innerHTML = "rentre des valeurs cohérentes";
            error.className = "error active";
            birthdate.className = "invalid";
            return false;
        }
    } else {
        error.innerHTML = "c'est pas une date";
        error.className = "error active";
        birthdate.className = "invalid";
        return false;
    }

};


let validElem = function (elem, regex, message, canEmpty, minLength, name, iderror, invRegex = false) {
    const error = document.getElementById(iderror);
    let test = regex.test(elem.value);

    if (invRegex === true) {
        test = !test;
    }

    if (elem.value.length < minLength) {
        error.innerHTML = name + " doit contenir au moins " + minLength + " caractères";
        error.className = "error active";
        elem.className = "invalid";
        return false;
    }

    if (!canEmpty) {
        if (elem.value.length === 0) {
            error.innerHTML = name + " ne peut etre vide";
            error.className = "error active";
            elem.className = "invalid";
            return false;
        }
    }
    if (test) {
        elem.className = "valid";
        error.innerHTML = "";
        error.className = "error";
        return true;
    } else {
        error.innerHTML = message;
        error.className = "error active";
        elem.className = "invalid";
        return false;
    }
};

let allvalid = function () {
    const mail = validElem(formContent.useremail, emailRegExp, "mail non valide", false, -1,
        "adresse mail", "error_mail");
    const lastname = validElem(formContent.lastname, /^.*$/, "nom non valide", false, -1,
        "nom", "error_last_name");
    const firstname = validElem(formContent.firstname, /^.*$/, "prenom non valide", false, -1,
        "prenom", "error_first_name");
    const username = validElem(formContent.username, /^.*$/, "nom d'utilisateur doit contenir au moins six caractèrese", false, 6,
        "identifiant", "error_id");
    const userpwd = validElem(formContent.userpwd, passwordRegExp, "mot de passe doit contenir au moins 8 caractère ainsi qu'une minuscule, une majuscule et un chiffre", false, -1,
        "mot de passe", "error_password", true);
    const birth = validDate(formContent.birthdate);
    return mail && lastname && firstname && username && userpwd && birth;
};


form.addEventListener("submit", function (event) {

    if (!allvalid()) {
        event.preventDefault();
    }
    console.log(formContent.lastname.value);
}, false);




