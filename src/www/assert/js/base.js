function openNav() {
    let navbar = document.getElementById("navbar");
    if (navbar.classList.contains("navShow")) {
        navbar.classList.remove("navShow");
    } else {
        navbar.classList.add("navShow");
    }
}

function showDropDownContentt() {
    document.getElementById("mydropdownn").classList.add("show");
}

function setWhiteTheme() {
    document.getElementById("body").classList.remove("dark_theme");
    document.getElementById("body").classList.add("white_theme");
}

function setDarkTheme() {
    document.getElementById("body").classList.add("dark_theme");
    document.getElementById("body").classList.remove("white_theme");
}

window.onclick = function (event) {
    if (!event.target.matches('.dropbtn')) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }
    }
}

document.getElementById("navBtn").addEventListener("click", openNav);
document.getElementById("dropBtn").addEventListener("click", showDropDownContentt);
document.getElementById("darkBtn").addEventListener("click", setDarkTheme);
document.getElementById("whiteBtn").addEventListener("click", setWhiteTheme);




