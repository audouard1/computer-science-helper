FROM continuumio/anaconda3

WORKDIR /usr/src/app

COPY ./src .

CMD [ "python", "./httpd" ]