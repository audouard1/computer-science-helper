# Computer Science Helper
[![Quality Gate Status](https://sonarqube.douard.me/api/project_badges/measure?project=ComputerScienseHelper&metric=alert_status)](https://sonarqube.douard.me/dashboard?id=ComputerScienseHelper)

Colors : [https://uidesigndaily.com/posts/sketch-ads-manager-table-list-day-1049](https://uidesigndaily.com/posts/sketch-ads-manager-table-list-day-1049)

Fonts : https://fonts.google.com/

Icons : https://fontawesome.com/icons?d=gallery&m=free

W3C Check HTML : https://validator.w3.org/nu/?doc=https%3A%2F%2Fcomputer-science-helper.douard.me%2Findex.html

W3C Check CSS : http://jigsaw.w3.org/css-validator/validator?uri=https%3A%2F%2Fcomputer-science-helper.douard.me%2Findex.html&profile=css3svg&usermedium=all&warning=1&vextwarning=&lang=fr

TP : http://lmbp.uca.fr/~barrel/tp2/

live démo : [https://computer-science-helper.douard.me/](https://computer-science-helper.douard.me/)

lien git : [https://gitlab.com/audouard1/computer-science-helper](https://gitlab.com/audouard1/computer-science-helper)

Site web HTML5 et CSS3 qui permet de remonter des articles interessants aux étudiants en informatique

TP réalisé par Anthony BERTRAND et Aurélien DOUARD


### étape 1
Structure hiérarchique dans le fichier sectionsHtml.pdf
liste a puce : /categories/Useful_tools/ExposeLocalEnv.html
image avec légende : /categories/Useful_tools/ExposeLocalEnv.html
tableau : /categories/Useful_tools/Blazor.html
lien intérieur : /index.html -> nav
lien extérieur : /index.html -> Ngrok
ancre : /index -> footer cliquer sur l'icone

### étape 2

reset css : normalize.css
flex box : ex nav 
pseudo format : hover, visited
transition : sur les balise `<a>` dans des articles
responsive : ex nav
 
### étape 3

/register.html 

### étape 4

/login.html

### étape 5

/chat.html

### étape 6

Affichage d'un bandeau sur la page "Home" qui indique à l'utilisateur l'utilisation de javascript, il disparaît au bout de 10s (même sans javascript)(jsbox.js)

Balise `<noscript>` et feuille de css (nojs.css) pour les navigateur sans javascript



